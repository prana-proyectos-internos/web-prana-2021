<?php
/* ===========================================
  Datos de conexion a la Base de Datos
============================================== */
// Server (online)
$db_database_online = 'db1prana';
$db_user_online   = 'ubd1prana';
$db_pass_online   = 'nr!n2FH5K';
$db_host_online   = 'localhost';
//LocalHost
$db_database_local  = 'db1prana';
$db_user_local    = 'root';
$db_pass_local    = 'root';
$db_host_local    = 'localhost';

/* ===========================================
  Seleccionar la conexión al servidor:
  0 = localhost
  1 = servidor online
============================================== */
define ('SERVIDOR', 0);

/* ===========================================
  Activar los Reportes de Errores en PHP
  0 = desactivado
  1 = activado
============================================== */
define ('DEBUG', 1);

/* ===========================================
  Activar HORARIO de Invierno
  0 = desactivado
  1 = activado
============================================== */
define ('HORARIO_INVIERNO', 0);

/* ===========================================
  Carpeta UPLOADS
============================================== */
// define ('UPLOAD', 'uploadz/');

/* ===========================================
	Ruta para llegar a la carpeta donde se
	encuentran los archivos Multimedias
============================================== */
define ( 'RUTA_TRABAJOS'	, 'uploads/' );

$web = [
  'url'         => 'https://prana.com.py/',
  'img'         => 'web.jpg',
  'titulo'      => 'PRANA',
  'titulo_2'    => '',
  'descripcion' => 'Colaboradores de marcas',
  'keywords'    => 'prana, agencia, campañas, paraguay, asunción',
  'autor'       => 'PRANA',
  'google_verification_id'  => 'fD0IUhMzv1QL_lshqX8tNjAlluZkZViVmPFzjSZ7UcI',
  'google_tag'              => '
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

          ga(\'create\', \'UA-111105160-3\', \'auto\');
          ga(\'send\', \'pageview\');
        </script>
    ',
    'google_fonts' => '<!-- Google FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;700&display=swap" rel="stylesheet">
    ',
    'twitter'       => '@pranapy',
    'twitter_url'   => 'https://twitter.com/pranapy',
    'facebook'      => '@prana.py',
    'facebook_url'  => 'https://www.facebook.com/prana.py'
];

// $web['img'] = 'yo.jpg' ;
// $web['descripcion'] = 'nueva descripcion' ;

