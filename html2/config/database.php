<?php

/* ===========================================

	Conexión a la BD a través del método PDO

============================================== */

class Database extends PDO
{
	public function __construct()
	{
		try
		{
			parent::__construct('mysql:host='.DB_HOST.';dbname='.DB_DATABASE, DB_USER, DB_PASS);
			parent::setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$this->query("SET NAMES 'utf8' COLLATE 'utf8_bin'");
			//Para que funcione con MySQL
			// $this->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");

		} catch (Exception $e){

			#die('La base de datos seleccionada no existe!');
			echo "Error: " . $e->getMessage();
			die();

		}
	}
}
// // Probamos una conexion
// $objData = new Database();
// if ($objData) {
// 	echo 'Conexión a la DB realizada con éxito';
// }
