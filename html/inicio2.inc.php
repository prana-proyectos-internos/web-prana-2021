<?php
// incluimos el archivo de inicio
require_once 'init.inc.php';

/* LIMITE DE TRABAJOS EN LA WEB */
// Preparamos la consulta
$objData = new Database();
$data = $objData->prepare('SELECT limite_trabajos_portada FROM configuraciones');
$data -> execute(); //Ejecutamos la consulta
$resultado_configuraciones = $data->fetch(PDO::FETCH_OBJ);
$limite_trabajos_portada = $resultado_configuraciones->limite_trabajos_portada;
/* ---------------------------------------------------------------------- */

// // Preparamos la consulta
$objData = new Database();
$sql ='SELECT
    id_trabajo,
    nombre_trabajo,
    fecha_lanzamiento,
    imagen_portada_trabajo,
    video_portada_trabajo,
    inicio_web,
    clientes_id_cliente,
    nombre_cliente,
    categorias_trabajo_id_categoria,
    nombre_categoria,
    estados_publicacion_id_estado_publicacion,
    nombre_estado_publicacion,
    fecha_registro,
    estado_registro
FROM trabajos
LEFT JOIN clientes ON trabajos.clientes_id_cliente = clientes.id_cliente
LEFT JOIN categorias_trabajo ON trabajos.categorias_trabajo_id_categoria = categorias_trabajo.id_categoria
LEFT JOIN estados_publicacion ON trabajos.estados_publicacion_id_estado_publicacion = estados_publicacion.id_estado_publicacion
WHERE inicio_web IS NOT null AND inicio_web > 0
AND estado_registro =1
ORDER BY inicio_web ASC LIMIT ';
$sql .= $limite_trabajos_portada;
$data = $objData->prepare($sql);
$data -> execute(); //Ejecutamos la consulta
$resultado_trabajos = $data->fetchAll(PDO::FETCH_OBJ);

f_D ( $resultado_trabajos );

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include_once 'partials/head.inc.php'; ?>
        <title><?= $web['titulo'] ?><?= $web['titulo_2'] ?></title>
    </head>

        <body class="bg-dark">
        
        <?php include_once 'partials/header.inc.php'; ?>

        <div class="content-wrap">
            <div class="swiper text-white swiper-full swiper-full-horizontal swiper-portfolio-animejs" data-swiper-slides="auto" data-swiper-gap="100" data-swiper-speed="600" data-swiper-loop="true" data-swiper-center="true" data-swiper-breakpoints="500:auto, 0:1">
                <div class="swiper-container">


                    <div class="swiper-wrapper">

                        <?php $i = 0; //Inicializamos el contador de TRABAJOS a mostrar en el HOME ?>

                        <?php foreach ( $resultado_trabajos as $value ) : ?>
                            <?php ++$i; //Aumentamos 1 ?>

                            <div class="swiper-slide" data-cursor-style="cursor-circle" data-cursor-text="ver">
                                <a class="card card-portfolio card-overlay card-image-sm card-bg-show text-white text-center" href="trabajo.view.php?id=<?= $value->id_trabajo ?>">
                                    <span class="card-img">
                                        <img src="uploads/<?= $value->imagen_portada_trabajo ?>" alt="<?= strtoupper( $value->nombre_trabajo ) ?>">
                                        <span class="background-color" style="background-color: rgba(14, 14, 14, .5);"></span>
                                    </span>
                                    <span class="card-img-overlay">
                                        <span class="card-title h2"><?= strtoupper( $value->nombre_trabajo ) ?></span>
                                        <span class="card-category h5 text-white"><?= strtoupper( $value->nombre_cliente ) ?></span>
                                        <span class="card-category subtitle"><?= f_text_uppercase ($value->nombre_categoria ) ?></span>
                                    </span>
                                </a>
                            </div>
                          
                        <?php endforeach ?>

                    </div>
                </div>
                <div class="swiper-button-wrapper container">
                    <div class="swiper-button-prev"><svg width='26' height='11' viewBox='0 0 26 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <path d='M5.5 1L1 5.5L5.5 10' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' />
                            <path d='M19 5.5H1' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' /></svg></div>
                    <div class="swiper-button-next"><svg width='26' height='11' viewBox='0 0 26 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
                            <path d='M20.5 1L25 5.5L20.5 10' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' />
                            <path d='M7 5.5H25' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' /></svg></div>
                </div>
                <div class="swiper-pagination"></div>
            </div>
        </div>
        <!-- START: Scripts -->
        <!-- Object Fit Polyfill -->
        <script src="assets/vendor/object-fit-images/dist/ofi.min.js"></script>
        <!-- Popper -->
        <script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrap Validator -->
        <script src="assets/vendor/bootstrap-validator/dist/validator.min.js"></script>
        <!-- ImagesLoaded -->
        <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <!-- Swiper -->
        <script src="assets/vendor/swiper/dist/js/swiper.min.js"></script>
        <!-- Animejs -->
        <script src="assets/vendor/animejs/lib/anime.min.js"></script>
        <!-- Rellax -->
        <script src="assets/vendor/rellax/rellax.min.js"></script>
        <!-- Countdown -->
        <script src="assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
        <!-- Moment.js -->
        <script src="assets/vendor/moment/min/moment.min.js"></script>
        <script src="assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
        <!-- Isotope -->
        <script src="assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/isotope-packery/packery-mode.pkgd.min.js"></script>
        <!-- Jarallax -->
        <script src="assets/vendor/jarallax/dist/jarallax.min.js"></script>
        <script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>
        <!-- Fancybox -->
        <script src="assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
        <!-- Themebau -->
        <script src="assets/js/themebau.min.js"></script>
        <!-- END: Scripts -->
    </body>
</html>