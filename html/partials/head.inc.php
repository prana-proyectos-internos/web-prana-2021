<?php if ( $web['google_tag'] ) {
	echo $web['google_tag'];
} ?>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="<?= $web['descripcion'] ?>">
        <meta name="author" content="<?= $web['autor'] ?>">
        <meta name="keywords" content="<?= $web['keywords'] ?>">
        <link rel="icon" type="image/png" href="assets/images/favicon.png">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- FavIco -->
        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
        <link rel="manifest" href="/site.webmanifest">
        <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#000">
        <meta name="msapplication-TileColor" content="#da532c">
        <meta name="theme-color" content="#ffffff">

        <!-- Facebook -->
        <meta property="og:url"         content="<?= $web['url'] ?>" />
        <meta property="og:type"        content="website" />
        <meta property="og:title"       content="<?= $web['titulo'] ?><?= $web['titulo_2'] ?>" />
        <meta property="og:description" content="<?= $web['descripcion'] ?>" />
        <meta property="og:image"       content="<?= $web['url'] ?><?= $web['img'] ?>" />

        <!-- Twitter -->
        <meta name="twitter:card"       content="summary" />
        <meta name="twitter:site"       content="<?= $web['twitter'] ?>" />
        <meta name="twitter:creator"    content="<?= $web['twitter'] ?>" />
        <meta property="og:url"         content="<?= $web['url'] ?>" />
        <meta property="og:title"       content="<?= $web['titulo'] ?><?= $web['titulo_2'] ?>" />
        <meta property="og:description" content="<?= $web['descripcion'] ?>" />
        <meta property="og:image"       content="<?= $web['url'] ?><?= $web['img'] ?>" />

        <?php if ( $web['google_fonts'] ) {
            echo $web['google_fonts'];
        } ?>

        <?php if ( $web['google_verification_id'] ) : ?><!-- Google SITE Verification -->
        <meta name="google-site-verification" content="<?= $web['google_verification_id'] ?>" /><?php endif ?>



        <!-- Estilos -->
        <!-- Swiper -->
        <link rel="stylesheet" href="assets/vendor/swiper/dist/css/swiper.min.css" />
        <!-- Fancybox -->
        <link rel="stylesheet" href="assets/vendor/fancybox/dist/jquery.fancybox.min.css" />
        <!-- Themebau -->
        <link rel="stylesheet" href="assets/css/themebau.min.css">
        <!-- Custom Styles -->
        <link rel="stylesheet" href="assets/css/custom.css">
        <!-- END: Styles -->

        <!-- jQuery -->
        <script src="assets/vendor/jquery/dist/jquery.min.js"></script>
        
