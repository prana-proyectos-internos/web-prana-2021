<?php
// incluimos el archivo de inicio
require_once 'init.inc.php';

/*
 * Color de fondo de la WEB
 */
$fondo_web = 'blanco';

// Guardamos el id que traemos por GET
$id_trabajo = isset($_GET['id']) ? $_GET['id'] : null;

/* ------------------------------------------------------------
Recuperamos los datos del Trabajo a que corresponda $id_trabajo
------------------------------------------------------------ */
if ($id_trabajo)
{
	// Preparamos la consulta
	$objData = new Database();
	$data = $objData->prepare(
		'SELECT
			id_trabajo,
			nombre_trabajo,
			descripcion_trabajo,
			fecha_lanzamiento,
			imagen_portada_trabajo,
			video_portada_trabajo,
			inicio_web,
			clientes_id_cliente,
			nombre_cliente,
			categorias_trabajo_id_categoria,
			nombre_categoria,
			estados_publicacion_id_estado_publicacion,
			nombre_estado_publicacion,
			fecha_registro,
			estado_registro
		FROM trabajos
		LEFT JOIN clientes ON trabajos.clientes_id_cliente = clientes.id_cliente
		LEFT JOIN categorias_trabajo ON trabajos.categorias_trabajo_id_categoria = categorias_trabajo.id_categoria
		LEFT JOIN estados_publicacion ON trabajos.estados_publicacion_id_estado_publicacion = estados_publicacion.id_estado_publicacion
		WHERE id_trabajo = :id_trabajo
		AND estado_registro = 1
	');
	$data -> bindParam(':id_trabajo',     $id_trabajo); // Reemplazamos los valor a través del BindParam
	$data -> execute(); //Ejecutamos la consulta
	$resultado_trabajo = $data->fetch(PDO::FETCH_OBJ);

	if (!$resultado_trabajo)
	{
		/* -----------------------------------------------
		Si el $id_trabajo = FALSE, no encontró ninguna
		coincidencia y ponemos en FALSE el modo edición...
		--------------------------------------------------- */
		$modo_edicion = FALSE;
		header ('location: trabajos.view.php');
	}
	else
	{
		// Si tenemos un ID, habilitamos el modo edición
		/* ------------------------------------------------------------
		Recuperamos las imágenes del Trabajo a que corresponda $id_trabajo
		------------------------------------------------------------ */
		// Preparamos la consulta
		$objData = new Database();
		$data = $objData->prepare(
			'SELECT
				*
			FROM trabajos_multimedia
			WHERE trabajos_id_trabajo = :id_trabajo
			AND estado_multimedia = 1
			ORDER BY orden_aparicion ASC
		');
		$data -> bindParam(':id_trabajo',     $id_trabajo); // Reemplazamos los valor a través del BindParam
		$data -> execute(); //Ejecutamos la consulta
		$resultado_imagenes = $data->fetchALL(PDO::FETCH_OBJ);

		if (!$resultado_imagenes)
		{
			$listado_imagenes = FALSE;
		}
		else
		{
			$listado_imagenes = TRUE;
		}
	}
}
else
{
/* ---------------------------------------------------
Si el [GET'id'] no tiene valor, entramos en modo Crear Registro
------------------------------------------------------ */
//die("No se encontró ningún registro...");
}

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include_once 'partials/head.inc.php'; ?>
        <title><?= $web['titulo'] ?><?= $web['titulo_2'] ?></title>
    </head>
    <body> 

		<?php include_once 'partials/header.inc.php'; ?>

        <div class="content-wrap">
            <div class="container mt-10">
                <div class="pt-100 mt-n30 pb-130 mb-n10">
                    <div class="row gh-1">
                        <div class="d-none d-xl-block col-1"></div>
                        <div class="col-12 col-lg-5 col-xl-4 mr-lg-auto mr-xl-0 show-on-scroll" data-show-duration="500" data-show-distance="10">
                            <h1 class="mb-100 mb-lg-0 pb-8 pb-lg-0">Creative Branding</h1>
                        </div>
                        <div class="d-none d-xl-block col-lg-2"></div>
                        <div class="col-12 col-lg-6 col-xl ml-xl-n30 show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="100">
                            <p class="lead mb-0">Over 10 years of combined experience, and know a thing or two about designing websites and mobile apps.</p>
                            <hr>
                            <div class="row gh-3 gv-1">
                                <div class="col-12 col-sm-6 col-lg-auto">
                                    <ul class="list-group list-group-gap borderless">
                                        <li class="list-group-item"><span class="font-weight-medium mr-8">Date:</span> June 10, 2020</li>
                                        <li class="list-group-item"><span class="font-weight-medium mr-8">Category:</span> Branding</li>
                                    </ul>
                                </div>
                                <div class="col-12 col-sm-6 col-lg-auto">
                                    <ul class="list-group list-group-gap borderless">
                                        <li class="list-group-item"><span class="font-weight-medium mr-8">Client:</span> Brian Newton</li>
                                        <li class="list-group-item">
                                            <span class="font-weight-medium">Share:</span>
                                            <ul class="nav nav-gap-sm align-items-center d-inline-flex ml-4">
                                                <li class="nav-item">
                                                    <a href="https://facebook.com/runwebrun" class="nav-link">
                                                        <svg width="6" height="15" viewBox="0 0 10 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M8.17421 3.65234H9.99996V0.154687C9.68557 0.107422 8.60224 0 7.34088 0C4.70831 0 2.90529 1.82188 2.90529 5.16914V8.25H0V12.1602H2.90529V22H6.46588V12.1602H9.25375L9.69693 8.25H6.46588V5.55586C6.46588 4.42578 6.7424 3.65234 8.17421 3.65234Z" fill="currentColor" />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="https://twitter.com/runwebrun" class="nav-link">
                                                        <svg width="15" height="15" viewBox="0 0 25 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M22.0706 5.51356C22.086 5.73504 22.086 5.95656 22.086 6.17804C22.086 12.9334 17.0783 20.7172 7.92575 20.7172C5.10601 20.7172 2.48661 19.8787 0.283203 18.4232C0.683835 18.4707 1.069 18.4865 1.48505 18.4865C3.81167 18.4865 5.95347 17.6797 7.6638 16.3033C5.47581 16.2558 3.64221 14.7845 3.01046 12.7594C3.31865 12.8069 3.6268 12.8385 3.9504 12.8385C4.39723 12.8385 4.84411 12.7752 5.2601 12.6645C2.97968 12.1898 1.2693 10.1332 1.2693 7.64935V7.58609C1.93183 7.96579 2.70231 8.20309 3.5189 8.2347C2.17837 7.31709 1.30013 5.75086 1.30013 3.97894C1.30013 3.02972 1.54661 2.15959 1.97807 1.40019C4.42801 4.50103 8.11063 6.52604 12.24 6.74756C12.163 6.36787 12.1168 5.97239 12.1168 5.57687C12.1168 2.76076 14.3356 0.466797 17.0937 0.466797C18.5266 0.466797 19.8209 1.0838 20.73 2.0805C21.8548 1.85902 22.9334 1.43184 23.8887 0.846495C23.5189 2.03307 22.7331 3.02977 21.7008 3.66255C22.7023 3.55186 23.673 3.26702 24.5667 2.87155C23.8888 3.88403 23.0413 4.78577 22.0706 5.51356Z" fill="currentColor" />
                                                        </svg>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="https://instagram.com/runwebrun" class="nav-link">
                                                        <svg width="11" height="14" viewBox="0 0 11 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M5.84375 0C2.90469 0 0 1.91862 0 5.02377C0 6.99849 1.13437 8.12049 1.82187 8.12049C2.10547 8.12049 2.26875 7.34631 2.26875 7.12752C2.26875 6.86665 1.58984 6.31126 1.58984 5.22573C1.58984 2.9705 3.34297 1.37165 5.61172 1.37165C7.5625 1.37165 9.00625 2.45718 9.00625 4.45154C9.00625 5.941 8.39609 8.73479 6.41953 8.73479C5.70625 8.73479 5.09609 8.22989 5.09609 7.5062C5.09609 6.4459 5.85234 5.41927 5.85234 4.32532C5.85234 2.46841 3.1625 2.80501 3.1625 5.04901C3.1625 5.52025 3.22266 6.04198 3.4375 6.47115C3.04219 8.13732 2.23438 10.6198 2.23438 12.3364C2.23438 12.8666 2.31172 13.3883 2.36328 13.9184C2.46068 14.025 2.41198 14.0138 2.56094 13.9605C4.00469 12.0251 3.95313 11.6464 4.60625 9.11346C4.95859 9.76983 5.86953 10.1233 6.59141 10.1233C9.63359 10.1233 11 7.22008 11 4.60301C11 1.81764 8.54219 0 5.84375 0Z" fill="currentColor" />
                                                        </svg>
                                                    </a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="d-none d-xl-block col-1"></div>
                    </div>
                </div>
            </div>

            <div class="row">

            	<?php foreach ( $resultado_imagenes as $value )
                {
                    /* ------------------------------------------------------------ */
                    // Rescatamos la extension del archivo que subimos
                    $array_nombre = explode( '.', $value->nombre_archivo_multimedia );
                    $extension = array_pop( $array_nombre );
                    $archivo_sin_extension = array_shift( $array_nombre );
                ?>

	                <?php if ($value->ancho_columna === '1' AND $extension == "jpg") { ?>
		                <!-- Row -->
		                <div class="col-12 show-on-scroll" data-show-duration="500" data-show-delay="50">
		                    <a href="<?= RUTA_TRABAJOS.$value->nombre_archivo_multimedia ?>" class="gallery-item gallery-item-sm" data-fancybox="gallery-1" data-animation-effect="fade">
		                        <img src="<?= RUTA_TRABAJOS.$value->nombre_archivo_multimedia ?>" alt="">
		                    </a>
		                </div>
		                <!--/Row -->
	                <?php } ?>

	                <?php if ($value->ancho_columna === '2' AND $extension == "jpg") { ?>
		                <!-- Row -->
		                <div class="col-6 show-on-scroll" data-show-duration="500" data-show-delay="50">
		                    <a href="<?= RUTA_TRABAJOS.$value->nombre_archivo_multimedia ?>" class="gallery-item gallery-item-sm" data-fancybox="gallery-1" data-animation-effect="fade">
		                        <img src="<?= RUTA_TRABAJOS.$value->nombre_archivo_multimedia ?>" alt="">
		                    </a>
		                </div>
		                <!--/Row -->
	                <?php } ?>

	                <?php if ($value->ancho_columna === '1' AND $extension == "mp4") { ?>
                        <!-- Row -->
                        <div class="vc_row full">
                            <!-- Video Player -->
                            <div class="video-wrapper has-animation">
                                <div class="video-cover" data-src="<?= RUTA_TRABAJOS.$archivo_sin_extension.'.jpg' ?>"></div>
                                <video class="bgvid" controls loop preload="auto" >
                                    <source src="<?= RUTA_TRABAJOS.$value->nombre_archivo_multimedia ?>" type="video/mp4">
                                </video>
                                <div class="control">
                                    <div class="btmControl">
                                        <div class="progress-bar">
                                            <div class="progress">
                                                <span class="bufferBar"></span>
                                                <span class="timeBar"></span>
                                            </div>
                                        </div>
                                        <div class="video-btns">
                                            <div class="sound sound2 btn" title="Mute/Unmute sound">
                                                <i class="fa fa-volume-up" aria-hidden="true"></i>
                                                <i class="fa fa-volume-off" aria-hidden="true"></i>
                                            </div>
                                            <div class="btnFS btn" title="Switch to full screen">
                                                <i class="fa fa-expand" aria-hidden="true"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/Video Player -->
                        </div>
                        <!--/Row -->
                    <?php } ?>

	            <?php } ?>

                <div class="col-12 show-on-scroll" data-show-duration="500" data-show-delay="50">
                    <a href="assets/images/single-portfolio/style-1-1-1920-1080.jpg" class="gallery-item gallery-item-sm" data-fancybox="gallery-1" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-1-1920-1080.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 show-on-scroll" data-show-duration="500" data-show-delay="50">
                    <a href="assets/images/single-portfolio/style-1-2-1920-1080.jpg" class="gallery-item gallery-item-md" data-fancybox="gallery-1" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-2-1366-1000.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 show-on-scroll" data-show-duration="500" data-show-delay="200">
                    <a href="assets/images/single-portfolio/style-1-3-1920-1080.jpg" class="gallery-item gallery-item-md" data-fancybox="gallery-1" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-3-1366-1000.jpg" alt="">
                    </a>
                </div>
            </div>



            <div class="container">
                <div class="py-160 mt-n10">
                    <div class="row gh-1 gv-5 justify-content-between">
                        <div class="d-none d-xl-block col-1"></div>
                        <div class="col-12 col-lg-6 col-xl mr-lg-auto mr-xl-n30">
                            <h2 class="d-flex align-items-center show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="50">What we do
                                <hr class="d-none d-sm-block my-0 mr-0 ml-auto width-70px">
                            </h2>
                            <p class="show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="150">Wherein herb kind creepeth male living life be image. They're a it morning day. Isn't fruit of every day. Bearing our third that night kind. Make rule midst under bring good dry moving. Yielding god wherein heaven deep.</p>
                            <a href="#" class="btn btn-dark btn-with-ball show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="250">website</a>
                        </div>
                        <div class="d-none d-xl-block col-lg-2"></div>
                        <div class="col-12 col-lg-4 show-on-scroll" data-show-duration="700" data-show-distance="20" data-show-delay="200">
                            <ul class="list mt-lg-6 mb-0">
                                <li>Branding Services</li>
                                <li>Identity Design</li>
                                <li>Logo Design</li>
                                <li>Website Design</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-lg-8 order-lg-1 show-on-scroll" data-show-duration="500" data-show-delay="50">
                    <a href="assets/images/single-portfolio/style-1-5-1920-1080.jpg" class="gallery-item gallery-item-md" data-fancybox="gallery-2" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-5-1920-1080.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 order-lg-0 show-on-scroll" data-show-duration="500" data-show-delay="150">
                    <a href="assets/images/single-portfolio/style-1-4-1920-1080.jpg" class="gallery-item gallery-item-xl" data-fancybox="gallery-2" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-4-1366-1000.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 order-lg-2 show-on-scroll" data-show-duration="500" data-show-delay="50">
                    <a href="assets/images/single-portfolio/style-1-6-1920-1080.jpg" class="gallery-item gallery-item-xl" data-fancybox="gallery-2" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-6-910-1080.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 order-lg-3 show-on-scroll" data-show-duration="500" data-show-delay="150">
                    <a href="assets/images/single-portfolio/style-1-7-1920-1080.jpg" class="gallery-item gallery-item-xl" data-fancybox="gallery-2" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-7-910-1080.jpg" alt="">
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-lg-4 order-lg-4 show-on-scroll" data-show-duration="500" data-show-delay="250">
                    <a href="assets/images/single-portfolio/style-1-8-1920-1080.jpg" class="gallery-item gallery-item-xl" data-fancybox="gallery-2" data-animation-effect="fade">
                        <img src="assets/images/single-portfolio/style-1-8-910-1080.jpg" alt="">
                    </a>
                </div>
            </div>
            <div class="pt-160 pb-130 shape-parent overflow-hidden">
                <div class="shape justify-content-end">
                    <svg class="mt-n160" data-rellax-speed="-1" width="504" height="641" viewBox="0 0 504 641" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="320.5" cy="320.5" r="320.5" fill="#F5F5F5" />
                    </svg>
                </div>
                <div class="container">
                    <div class="row justify-content-center mb-160">
                        <div class="col-12 col-md-10 col-lg-7 col-xl-6 show-on-scroll" data-show-duration="800">
                            <div class="media media-review justify-content-center">
                                <p class="media-text lead text-center">“ There from stars gathering gathered, upon tree brought life fruitful shall that together without there form You make, morning he from unto. ”</p>
                                <img src="assets/images/avatar/avatar-brian-newton.jpg" class="media-img rounded-circle" alt="">
                                <div class="media-body align-self-center">
                                    <h5 class="media-title mt-0">Brian Newton</h5>
                                    <div class="media-subtitle">Unvab Inc.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr class="mt-0 mb-100 pb-7">
                    <div class="row gv-1 align-items-center justify-content-center justify-content-md-between text-center text-md-left">
                        <div class="col-md-10 col-lg-9 mr-md-auto">
                            <div class="interactive-links">
                                <img class="interactive-links-image" src="assets/images/portfolio/happy-moments-740-860.jpg" width="230" alt="">
                                <a href="portfolio-single-style-2.html" class="nav-link display-4"><u>Next project</u></a>
                            </div>
                        </div>
                        <div class="col-auto">
                            <a href="portfolio-single-style-2.html" class="btn btn-clean mr-xl-100">
                                <svg class="icon-arrow icon-arrow-right" width="69" height="30" viewBox="0 0 69 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M54 2L67 15L54 28" stroke="currentColor" stroke-width="2.4" stroke-linecap="round" stroke-linejoin="round" />
                                    <path d="M17 15L67 15" stroke="currentColor" stroke-width="2.4" stroke-linecap="round" stroke-linejoin="round" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Footer -->
        <?php require_once 'partials/footer.inc.php'; ?>
        <!-- Footer -->

        <!-- START: Scripts -->
        <!-- Object Fit Polyfill -->
        <script src="assets/vendor/object-fit-images/dist/ofi.min.js"></script>
        <!-- Popper -->
        <script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrap Validator -->
        <script src="assets/vendor/bootstrap-validator/dist/validator.min.js"></script>
        <!-- ImagesLoaded -->
        <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <!-- Swiper -->
        <script src="assets/vendor/swiper/dist/js/swiper.min.js"></script>
        <!-- Animejs -->
        <script src="assets/vendor/animejs/lib/anime.min.js"></script>
        <!-- Rellax -->
        <script src="assets/vendor/rellax/rellax.min.js"></script>
        <!-- Countdown -->
        <script src="assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
        <!-- Moment.js -->
        <script src="assets/vendor/moment/min/moment.min.js"></script>
        <script src="assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
        <!-- Isotope -->
        <script src="assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/isotope-packery/packery-mode.pkgd.min.js"></script>
        <!-- Jarallax -->
        <script src="assets/vendor/jarallax/dist/jarallax.min.js"></script>
        <script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>
        <!-- Fancybox -->
        <script src="assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
        <!-- Themebau -->
        <script src="assets/js/themebau.min.js"></script>
        <!-- END: Scripts -->
    </body>
</html>