<?php
$noticia_1 = ( object ) [
    'id_noticia'        => '1',
    'fecha_noticia'     => '2020-06-01',
    'imagen_noticia'    => 'equipo_prana.jpg',
    'titulo_noticia'    => 'Equipo apasionado por la comunicación',
    'bajada_noticia'    => 'Borrar la línea entre lo digital y lo analógico, conectando al consumidor con conversaciones que trasciendan cualquier medio. Esta es la filosofía de trabajo de Prana, una agencia que avanza conforme a los cambios que dicta el mercado, con ideas innovadoras acordes a sus estrategias.',
    'autor_noticia'     => 'Laura Leguizamón',
    'autor_cargo'       => 'Gerente General',
    'cuerpo_noticia'    => '<p>En el departamento de Marketing de Tabacos del Paraguay comenzaba a gestarse en el año 1998 una agencia que se convertiría en una de las más fuertes del país: Prana, operando como un subdepartamento de diseño y creatividad.</p>
    <p>En poco tiempo otras marcas del grupo solicitaron sus servicios y más adelante también golpearon la puerta clientes externos. Fue entonces cuando se dieron cuenta de la capacidad del equipo y de la calidad del trabajo que realizaban, lo que les hizo dar el salto para comenzar a crecer, convirtiéndose en ese entonces en una pequeña agencia en cuanto a estructura para más adelante independizarse y formar lo que es actualmente Prana.</p>
    <p>De sus inicios a lo que es ahora, Prana experimentó un crecimiento paulatino reflejado en el volumen de trabajo, la cantidad de colaboradores, los mismos departamentos que pasaron de ser solo de diseño y creatividad a manejar medios, digital, PR, etc. Pero, más que nada, lograron en todos estos años una identidad propia y un nivel de calidad en todo lo que hacen.</p>
    <p>“El cambio fue muy grande, las marcas pasaron de un monólogo en el que solo ellas emitían un mensaje a empezar a dialogar con sus consumidores. Las marcas ya no hablan solo de ellas y sus cualidades, sino que deben generar contenido atractivo y hablar a su público de lo que les interesa para poder comenzar ese diálogo, ese es el gran desafío”, explicó Laura Leguizamón, gerente general de la agencia.</p>
    <p>El equipo de Prana está conformado por 52 personas que trabajan en los más variados departamentos: creatividad, diseño, cuentas, planning, producción, administración, medios, PR y digital.</p>
    <p>A través de estos, ofrece servicios completamente integrales para marcas, empresas e industrias de diferentes categorías de productos y servicios que la contactan. “Cuando llega un nuevo cliente a Prana nos interiorizamos sobre cada negocio para trabajar las marcas con una mirada estratégica. No buscamos ser solo una agencia que trabaja sobre pedidos, sino involucrarnos realmente con los clientes y proponer acciones de forma proactiva”, señaló Laura.</p>
    <p>Por otra parte, dijo que el mayor logro de la empresa se da cuando un cliente está satisfecho con lo que recibe, y más que nada, cuando lo que hacen desde la agencia ayuda a que sus clientes alcancen sus objetivos.</p>
    <p>Actualización permanente. Prana no se queda en “el molde ni en el tiempo”, avanza conforme van sucediendo los cambios. Es una agencia con colaboradores inquietos que van en busca de nuevas tendencias. Estas virtudes son consideradas claves para una agencia que busca acompañar los negocios de sus clientes con ideas innovadoras acordes a sus estrategias para generar resultados positivos y tangibles.</p>
    <p>Para llevar adelante sus metas, Prana trabaja de manera colaborativa con partners que suman su expertise y especialización en varios campos. “Según el requerimiento de cada proyecto, recurrimos a ellos para dar a nuestros clientes un trabajo hecho por especialistas en cada campo”, indicó.</p>
    <p>Hay mucho foco en lo digital, sin dudas, pero también es clave el acompañamiento en la construcción de la estrategia para que la comunicación se alinee a esta y logre verdaderamente impacto en los resultados. En Prana buscamos siempre anclar las ideas en la estrategia y que pueda medirse en resultados, debe haber un retorno de inversión para que hablemos de un trabajo exitoso.</p>
    <p>Finalmente, Laura mencionó que tienen previsto encarar el 2020 pensando en positivo. Y lo harán de la mano de un gran equipo de profesionales, de una filosofía consolidada, experiencia y muchas ganas de hacer proyectos exitosos, sumado a la certeza de que manejan grandes marcas a las que acompañarán para que sigan creciendo y obteniendo buenos resultados , concluyó la gerente general.</p>',
    'quote_noticia'    => null
];
$noticia_2 = ( object ) [
    'id_noticia'        => '2',
    'fecha_noticia'     => '2020-12-07',
    'imagen_noticia'    => 'jorge_buffa_prana.jpg',
    'titulo_noticia'    => '¿Es suficiente tener una tienda virtual para vender más?',
    'bajada_noticia'    => 'La situación actual de pandemia mundial nos obliga a buscar soluciones, para seguir haciendo con relativa normalidad lo que veníamos realizando anteriormente.',
    'autor_noticia'     => 'Jorge Buffa',
    'autor_cargo'       => 'Director Desarrollo y Nuevas Tecnologías',
    'cuerpo_noticia'    => '<p>En este contexto las marcas buscan alternativas para salir adelante. Una de ellas es montar una tienda virtual. Pero ¿tener un comercio electrónico funcionando correctamente es suficiente para aumentar las ventas? Para encontrar una respuesta primero hagamos un ejercicio rápido, de algunos componentes que necesitamos para la puesta en marcha de nuestra tienda virtual.</p>
    <p>Lo primero que tenemos que asumir es que tenemos un producto de calidad, cuidado al máximo y de los mejores del mercado. Pero ofrecer y tener lo mejor, no siempre se traduce en que nuestra tienda sea exitosa. Sabemos que tenemos el mejor producto, pero necesitamos un lugar dónde ofrecerlo y decidimos que una tienda virtual es la mejor opción. Esta es la herramienta tecnológica que nos permitirá empezar a vender digitalmente.</p>
    <p>Pensemos por un momento que estamos ante una tienda virtual que funciona al 100%: programación depurada y funcional, codificación limpia, pasarela de pagos en funcionamiento, diseño web que se adapta a todos los dispositivos (celulares, tabletas, computadoras de escritorio), carga rápida de nuestra web y experiencia de usuario agradable. Si logramos alcanzar todo esto, podemos decir que tenemos en nuestras manos la herramienta digital perfecta para impulsar nuestras ventas.</p>
    <h4>¿Es suficiente?</h4> 
    <p>Aún no, y para entender esto hagamos un pequeño paralelismo con una tienda física: aunque parezca una obviedad, tener una tienda limpia, ordenada, decorada, con estantes llenos de productos en buen estado, generar un ambiente agradable para el cliente y que se sienta cómodo al visitarnos, todo esto no basta. Nos falta un componente fundamental. Si llegamos hasta este punto, es porque tenemos una plataforma ágil, amigable para el usuario y lista para utilizarla. Pero…</p>
    <h4>Equipo humano preparado:</h4> 
    <p>Al igual que una tienda física, donde tenemos personas preparadas para recibir y atender a los clientes, donde se genera una interacción, lo mismo sucede en nuestra tienda virtual. Tal vez caigamos en el error de pensar que tener la plataforma funcionando correctamente y de manera automática es suficiente. Pero olvidamos que aun así se genera una interacción virtual entre el visitante y nuestro negocio.</p>
    <p>Necesitamos realizar algunos ajustes en nuestra empresa para poder mejorar las ventas. Cada decisión va acompañada de un ajuste interno dentro de la misma. ¿Qué talentos humanos necesitamos para lograr el funcionamiento y resultados trazados? No tendría por qué ser diferente si decidimos tener una tienda virtual. Debemos dotar a esta herramienta responsabilidades y funciones, que sean atendidas por personal calificado y entrenado. Parece un contrasentido pensar que algo tan actual y tecnológico, siga necesitando la intervención humana, pero es importante entender que siempre habrá que hacerle un seguimiento a lo que pueda estar pasando en nuestra plataforma.</p>
    <p>La experiencia de compra lo es todo: Tomamos como ejemplo el envío de nuestro producto una vez que realizamos la venta. Si nuestra logística de envío falla, llega fuera del tiempo que nosotros mismos establecíamos, o peor aún no llega nunca, esto se traduce en una experiencia de compra insatisfactoria y perdemos la confianza del cliente. Llegar en tiempo y forma hasta la puerta del cliente, es tan importante como que funcione correctamente nuestro sitio web.</p>
    <p>Finalmente, es fundamental alimentar el tráfico a nuestro sitio. No podemos asumir que porque cumplimos con la puesta online de nuestra tienda, eso se traduce automáticamente en visitas. Si volvemos al paralelismo con una tienda física, no basta con abrir las puertas de la misma, tenemos que captar la atención del posible comprador y llevarlo de la mano si es posible hasta nuestra tienda. Una adecuada campaña de marketing digital nos dará la posibilidad de que nos descubran, utilizando plataformas como Facebook Ads, Instagram, Google Display, Compra programática, banners en medios digitales y otros. La pauta digital es imprescindible para nuestra plataforma.</p>
    <h4>Tener una tienda virtual hoy en día es fundamental y ya no es un lujo</h4>
    <p>Pero toda herramienta mal utilizada es un desperdicio, si no tiene un acompañamiento y gestión para que ésta sea sustentable en el tiempo y cumpla su cometido, que es aumentar las ventas a través de este canal. Se necesita una evaluación constante de la experiencia de compra en nuestra tienda virtual. Eso implica un pensamiento estratégico comercial, más allá del uso en sí de la tecnología. Así trabajamos en Prana, con espíritu colaborativo con los objetivos de las marcas.</p>',
    'quote_noticia'    => 'Parece un contrasentido pensar que algo tan actual y tecnológico, siga necesitando la intervención humana'
];
$noticia_3 = ( object ) [
    'id_noticia'        => '3',
    'fecha_noticia'     => '2020-01-04',
    'imagen_noticia'    => 'ale_duarte_prana.jpg',
    'titulo_noticia'    => 'El paso de lo tradicional a lo digital',
    'bajada_noticia'    => 'Esta nueva normalidad, nos plantea el desafío de ajustar todas nuestras estrategias de comunicación.',
    'autor_noticia'     => 'Alejandra Duarte',
    'autor_cargo'       => 'Directora Digital',
    'cuerpo_noticia'    => '<p>Cada día nos interpela para buscar una conexión entre la marca y el consumidor en la que se combinan lo tradicional y lo digital. Hay que apostar a desarrollar puntos claves a la hora de trabajar una estrategia digital, comenzando en cómo funciona el negocio, cómo se plantea la inversión digital y la estrategia de contenido.</p>
    <p>El área digital es una de las más fortalecidas durante esta crisis sanitaria, sobre todo en ámbitos como el e-commerce, las redes sociales y el marketing de contenidos. En Paraguay se dio un paso gigantesco en este periodo y esto lo observamos en nuestros clientes, pero por sobre todo en las pymes. En este proceso de volvernos digitales nos tocó a todos reinventarnos, desde las clases online, la venta digital y hasta como administrativa o logísticamente se lograba digitalizar ese proceso. Los ciudadanos en general aprendimos de manera forzada, pero nos ayudó a evolucionar de manera positiva.</p>
    <p>Debemos tener en cuenta que la forma de comunicación cambió. Necesitamos tomarnos el tiempo para revisar e innovar nuestras estrategias de marketing. Es un proceso constante donde las expectativas de los clientes son altas, hacia las acciones de la marca y la experiencia que deben recibir. Los consumidores hoy son más exigentes y reclaman estrategias en las que el centro sean ellos.</p>
    <p>Nuestra recomendación desde Prana, a la hora de analizar lo que deben tener en cuenta las empresas en sus estrategias digitales, son los siguientes aspectos:</p>
    <h4>Cambios de comportamiento en el usuario</h4>
    <p>Esta pandemia nos propone estar en un constante movimiento de acciones, ya que las conductas de la sociedad están siendo cada vez más afectadas por el distanciamiento social y por aislamiento.</p>
    <p>Supone en ello estar preparados. Se produjo el aceleramiento en el proceso de lo tradicional a lo digital, en distintos ámbitos de la sociedad. Por lo tanto se producen cambios en el comportamiento del usuario.</p>
    <h4>Generar contenido de interés</h4>
    <p>Dentro de los cambios que se han generado es darse cuenta de que el tiempo asignado a las tareas cotidianas, ya no es un factor que organizaban terceros. Estar en casa hace para muchos que el tiempo pase más lento, se comparte el espacio entre oficina y casa. No olvidemos que el tener asignado y controlado por terceros las horas de entrada, almuerzo y salida, nos ayudaban a una organización de cierto tiempo libre. Hoy que no contamos con esto, nos vemos en la necesidad de adquirir este control.</p>
    <p>Para ello se van generando espacios de distracción o de “desconexión” de las actividades cotidianas para dar lugar al entretenimiento.</p>
    <p>Es aquí, entonces, donde debemos poner el enfoque para poder acceder a ese tiempo y ofrecer una gama de soluciones o alternativas para esos consumidores. Encontrar la mejor forma de conectarse. Ya no hay empresas grandes o pequeñas, sino solo aquellas que están dispuestas a adaptarse a un nuevo mundo, para dar el paso de lo tradicional al mundo digital.</p>',
    'quote_noticia'    => 'Es un proceso constante donde las expectativas de los clientes son altas, hacia las acciones de la marca y la experiencia que deben recibir'
];

$noticias = [
    $noticia_1,
    $noticia_2,
    $noticia_3
];

// var_dump ( $noticias );