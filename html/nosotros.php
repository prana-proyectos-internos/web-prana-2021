<?php
// incluimos el archivo de inicio
require_once 'init.inc.php';

/*
 * Color de fondo de la WEB
 */
$fondo_web = 'negro';

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include_once 'partials/head.inc.php'; ?>
        <title><?= $web['titulo'] ?><?= $web['titulo_2'] ?></title>
    </head>

    <body>

        <?php include_once 'partials/header.inc.php'; ?>

        <div class="content-wrap nosotros-content">
            <div class="py-160 min-vh-100 d-flex align-items-center position-relative lines-style-3">
                <div class="line text-white"></div>
                <div class="background bg-dark">
                    <div class="background-image jarallax" data-jarallax data-speed="0.8">
                        <img src="imagenes/nosotros1.jpg" class="jarallax-img" alt="">
                    </div>
                    <div class="background-color" style="background-color: #0e0e0e; opacity: .2;"></div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-1"></div>
                        <div class="col-12 col-lg-8 show-on-scroll" data-show-duration="700">
                            <h1 class="text-white mb-0">Nosotros</h1>
                            <h3 class="text-white">¿Quienes Somos?</h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="py-160 shape-parent overflow-hidden">
                <div class="shape justify-content-end">
                    <svg class="mr-n90 mt-n160" width="641" height="641" viewBox="0 0 641 641" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <circle cx="320.5" cy="320.5" r="320.5" fill="#F5F5F5" />
                    </svg>
                </div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-12 col-lg-10">
                            <div class="row gh-5 gv-5">
                                <div class="col-12 col-md-10 col-lg-9">
                                    <p class="subtitle mt-n10 show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="50">Nosotros</p>
                                    <h3 class="mb-n7 show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="150">Prana S.A. 👋</h3>
                                </div>
                                <div class="col-12 col-md-6 show-on-scroll" data-show-duration="700">
                                    <p class="lead mb-30">Una agencia que nació como una pequeña parte de una empresa, y que creció tanto, que ahora impulsa las marcas de uno de los grupos económicos más grandes de Paraguay.</p>
                                    <img class="w-100 mt-100" src="imagenes/nosotros/n1.jpg" alt="">
                                </div>
                                <div class="col-12 col-md-6 show-on-scroll" data-show-duration="700">
                                    <div class="swiper swiper-button-style-3 mt-7" data-swiper-slides="1" data-swiper-speed="600" data-swiper-loop="true" data-swiper-parallax="true">
                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide overflow-hidden">
                                                    <div class="swiper-image" data-swiper-parallax-x="20%">
                                                        <img class="w-100" src="imagenes/nosotros/n2.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="swiper-slide overflow-hidden">
                                                    <div class="swiper-image" data-swiper-parallax-x="20%">
                                                        <img class="w-100" src="imagenes/nosotros/n3.jpg" alt="">
                                                    </div>
                                                </div>
                                                <div class="swiper-slide overflow-hidden">
                                                    <div class="swiper-image" data-swiper-parallax-x="20%">
                                                        <img class="w-100" src="imagenes/nosotros/n4.jpg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="swiper-button-next bg-white"><svg width='26' height='11' viewBox='0 0 26 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                                <path d='M20.5 1L25 5.5L20.5 10' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' />
                                                <path d='M7 5.5H25' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' /></svg></div>
                                        <div class="swiper-button-prev bg-white"><svg width='26' height='11' viewBox='0 0 26 11' fill='none' xmlns='http://www.w3.org/2000/svg'>
                                                <path d='M5.5 1L1 5.5L5.5 10' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' />
                                                <path d='M19 5.5H1' stroke='currentColor' stroke-width='1.3' stroke-linecap='round' stroke-linejoin='round' /></svg></div>
                                    </div>
                                    <p class="pt-3 mt-100 mb-0">Un equipo de más de 50 personas que constantemente busca renovarse y capacitarse, en alianza con Escuela de Creativos de Argentina como plan de capacitación. <br> Trabajemos juntos. Si tenés un negocio, nosotros tenemos ideas para hacerlo crecer. </p>
                                </div>
                            </div>
                            <hr class="mt-160 pb-8">
                            <div class="mb-n7">
                                <div class="row gv-2 justify-content-around text-center">
                                    <div class="col-sm-6 col-md-auto">
                                        <div class="number-box show-on-scroll" data-show-duration="500" data-show-distance="15" data-show-delay="50">
                                            <div class="number-box-title h1">20</div>
                                            <div class="number-box-subtitle">años de experiencia</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-auto">
                                        <div class="number-box show-on-scroll" data-show-duration="500" data-show-distance="15" data-show-delay="150">
                                            <div class="number-box-title h1">+40</div>
                                            <div class="number-box-subtitle">marcas en el portfolio</div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-auto">
                                        <div class="number-box show-on-scroll" data-show-duration="500" data-show-distance="15" data-show-delay="250">
                                            <div class="number-box-title h1">top 5</div>
                                            <div class="number-box-subtitle">agenicas del país el los últimos 5 años</div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-sm-6 col-md-auto">
                                        <div class="number-box show-on-scroll" data-show-duration="500" data-show-distance="15" data-show-delay="350">
                                            <div class="number-box-title h1">9</div>
                                            <div class="number-box-subtitle">awards</div>
                                        </div>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
        

        <!-- Footer -->
        <?php require_once 'partials/footer.inc.php'; ?>
        <!-- Footer -->


        <!-- START: Scripts -->
        <!-- Object Fit Polyfill -->
        <script src="assets/vendor/object-fit-images/dist/ofi.min.js"></script>
        <!-- Popper -->
        <script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrap Validator -->
        <script src="assets/vendor/bootstrap-validator/dist/validator.min.js"></script>
        <!-- ImagesLoaded -->
        <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <!-- Swiper -->
        <script src="assets/vendor/swiper/dist/js/swiper.min.js"></script>
        <!-- Animejs -->
        <script src="assets/vendor/animejs/lib/anime.min.js"></script>
        <!-- Rellax -->
        <script src="assets/vendor/rellax/rellax.min.js"></script>
        <!-- Countdown -->
        <script src="assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
        <!-- Moment.js -->
        <script src="assets/vendor/moment/min/moment.min.js"></script>
        <script src="assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
        <!-- Isotope -->
        <script src="assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/isotope-packery/packery-mode.pkgd.min.js"></script>
        <!-- Jarallax -->
        <script src="assets/vendor/jarallax/dist/jarallax.min.js"></script>
        <script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>
        <!-- Fancybox -->
        <script src="assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
        <!-- Themebau -->
        <script src="assets/js/themebau.min.js"></script>
        <!-- END: Scripts -->
    </body>
</html>