<?php

/*
 * Convertir cadena de TEXTO a UPPERCASE
 */
function f_text_uppercase ( $valor ) 
{
	$valor_convertido = mb_strtoupper( $valor, 'UTF-8' );
	return $valor_convertido;
};

/*
 * Die and Dump
 */
function f_DD ( $value )
{
	// Función die and dump
	echo '<pre>';
	return die( var_dump( $value ) );
}

/*
 * Dump
 */
function f_D ( $value )
{
	// Función die and dump
	echo '<pre>';
	return ( var_dump( $value ) );
	echo '</pre>';
}
