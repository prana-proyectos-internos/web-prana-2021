<?php
/* ===========================================
  RUTAS DEL SITIO
============================================== */
$rutas = [
	[
		'url'		=> 'nosotros.php',
		'boton'		=> 'Nosotros',
		'parametro' => '',
		$sub_menu = []
	],
	[
		'url'		=> 'trabajos.php',
		'boton'		=> 'Trabajos',
		'parametro' => ''
	],
	[
		'url'		=> 'unidades.php',
		'boton'		=> 'Unidades',
		'parametro' => ''
	],
	[
		'url'		=> 'clientes.php',
		'boton'		=> 'Clientes',
		'parametro' => ''
	],
	[
		'url'		=> 'colaboradores.php',
		'boton'		=> 'Colaboradores',
		'parametro' => ''
	],
	[
		'url'		=> 'contacto.php',
		'boton'		=> 'Contacto',
		'parametro' => ''
	],
];