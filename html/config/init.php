<?php
/* --------------------------------------------
Archivo de INICIALIZACIÓN...
********* NO EDITAR ESTE Archivo *************
Las configuraciones están en configuraciones.php
------------------------------------------------ */
require 'configuraciones.php';

// Conexión al Servidor Local o al onLine
if ( SERVIDOR == 1 ) {
  define ( 'DB_DATABASE'  , $db_database_online );
	define ( 'DB_USER'      , $db_user_online );
	define ( 'DB_PASS'      , $db_pass_online );
	define ( 'DB_HOST'      , $db_host_online );
} else {
  define ( 'DB_DATABASE'  , $db_database_local );
	define ( 'DB_USER'      , $db_user_local );
	define ( 'DB_PASS'      , $db_pass_local );
	define ( 'DB_HOST'      , $db_host_local );
}

// Habilitamos o NO los Errores
if (DEBUG == 1 ) {
  // Habilitamos todos los reportes
  error_reporting(E_ALL);
  ini_set( 'display_errors', 1 );
} else {
  // Deshabilitamos los reportes
  error_reporting(0);
  ini_set( 'display_errors', 0 );
}

// Zona y lenguaje
date_default_timezone_set('America/Asuncion');
setlocale(LC_ALL, 'es-PY');



