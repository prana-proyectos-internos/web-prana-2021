<?php
/* ===========================================
  Datos de conexion a la Base de Datos
============================================== */
// Server (online)
$db_database_online	= 'db157136_combos';
$db_user_online		= 'db157136_cqa';
$db_pass_online		= 'hwbU9}7@U-ycqa';
$db_host_online		= 'internal-db.s157136.gridserver.com';
//LocalHost
$db_database_local	= 'db1prana';
$db_user_local		= 'root';
$db_pass_local		= 'root';
$db_host_local		= 'localhost';

/* ===========================================
  Seleccionar la conexión al servidor:
  0 = localhost
  1 = servidor online
============================================== */
define ('SERVIDOR', 0);

/* ===========================================
  Activar los Reportes de Errores en PHP
  0 = desactivado
  1 = activado
============================================== */
define ('DEBUG', 1);

/* ===========================================
  Activar HORARIO de Invierno
  0 = desactivado
  1 = activado
============================================== */
define ('HORARIO_INVIERNO', 0);

/* ===========================================
  Carpeta UPLOADS
============================================== */
// define ('UPLOAD', 'uploadz/');

/* ===========================================
	Ruta para llegar a la carpeta donde se
	encuentran los archivos Multimedias
============================================== */
define ( 'RUTA_TRABAJOS'	, 'uploads/' );

/* ===========================================
  FONDO por defecto de la WEB
  Parametros = [blanco] [negro]
============================================== */
$fondo_web = 'negro';


/* ===========================================
  CONFIGURACIONES DEL SITIO
============================================== */
$web = [
  'url'         => 'https://prana.com.py/',
  'email'       => 'hola@prana.com.py',
  'email_text'  => 'Hola!%20me%20gustaría%20saber%20más%20información%20acerca%20de%20Prana.',
  'direccion'   => '1250, José a Moreno González,',
  'ciudad_pais' => 'Asunción, Paraguay',
  'telefono'    => '+595 21-660-538',
  'img'         => 'web.jpg',
  'titulo'      => 'PRANA',
  'titulo_2'    => '',
  'descripcion' => 'Somos una agencia que nació como una pequeña parte de una empresa, y que creció tanto, que ahora impulsa las marcas de uno de los grupos económicos más grandes de Paraguay.',
  'keywords'    => 'prana, agencia, campañas, paraguay, asunción',
  'autor'       => 'PRANA',
  'google_verification_id'  => 'fD0IUhMzv1QL_lshqX8tNjAlluZkZViVmPFzjSZ7UcI',
  'google_tag'              => '
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script>
          (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,\'script\',\'https://www.google-analytics.com/analytics.js\',\'ga\');

          ga(\'create\', \'UA-111105160-3\', \'auto\');
          ga(\'send\', \'pageview\');
        </script>
    ',
    'google_fonts' => '<!-- Google FONTS -->
        <link href="https://fonts.googleapis.com/css2?family=Archivo+Black&family=Archivo:ital,wght@0,400;0,600;1,400;1,600&display=swap" rel="stylesheet">
    ',
    'twitter'       => '@pranapy',
    'twitter_url'   => 'https://twitter.com/pranapy',
    'facebook'      => '@prana.py',
    'facebook_url'  => 'https://www.facebook.com/prana.py',
    'instagram'      => '@pranapy',
    'instagram_url'  => 'https://www.instagram.com/pranapy/',
    'linkedin'      => '',
    'linkedin_url'  => 'https://www.linkedin.com/company/pranapy/'
];

// $web['img'] = 'yo.jpg' ;
// $web['descripcion'] = 'nueva descripcion' ;

