<?php
// incluimos el archivo de inicio
require_once 'init.inc.php';

/*
 * Color de fondo de la WEB
 */
$fondo_web = 'blanco';

// Preparamos la consulta
$objData = new Database();
$data = $objData->prepare(
	'SELECT
		id_trabajo,
		nombre_trabajo,
		descripcion_trabajo,
		fecha_lanzamiento,
		imagen_portada_trabajo,
		video_portada_trabajo,
		inicio_web,
		clientes_id_cliente,
		nombre_cliente,
		categorias_trabajo_id_categoria,
		nombre_categoria,
        url_categoria,
        unidades_trabajo_id_unidad_trabajo,
        nombre_unidad_trabajo,
        url_unidad_trabajo,
		estados_publicacion_id_estado_publicacion,
		nombre_estado_publicacion,
		fecha_registro,
		estado_registro
	FROM trabajos
	LEFT JOIN clientes ON trabajos.clientes_id_cliente = clientes.id_cliente
	LEFT JOIN categorias_trabajo ON trabajos.categorias_trabajo_id_categoria = categorias_trabajo.id_categoria
	LEFT JOIN unidades_trabajo ON trabajos.unidades_trabajo_id_unidad_trabajo = unidades_trabajo.id_unidad_trabajo
	LEFT JOIN estados_publicacion ON trabajos.estados_publicacion_id_estado_publicacion = estados_publicacion.id_estado_publicacion
    WHERE estado_registro =1
	ORDER BY inicio_web ASC
');
$data -> execute(); //Ejecutamos la consulta
$resultado_trabajos = $data->fetchAll(PDO::FETCH_OBJ);

// var_dump($resultado_trabajos);
// die;

/* --------------------------------------------------
Consultar las CATEGORIAS que tenemos en la BD
----------------------------------------------------- */
$objData = new Database();
$data = $objData->prepare('SELECT * FROM categorias_trabajo');
$data -> execute(); //Ejecutamos la consulta
$resultado_categorias = $data->fetchAll(PDO::FETCH_OBJ);

?>
<!DOCTYPE html>
<html lang="es">
    <head>
        <?php include_once 'partials/head.inc.php'; ?>
        <title><?= $web['titulo'] ?><?= $web['titulo_2'] ?></title>
    </head>
    <body>
        

		<?php include_once 'partials/header.inc.php'; ?>


        <div class="content-wrap">
            <div class="container isotope mt-130">
                <ul class="nav justify-content-center isotope-options mb-60 show-on-scroll" data-show-duration="500">
                    <li class="nav-item active show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="100">
                        <a href="#" data-filter="all" class="nav-link">
                            <div class="nav-link-name small">todos</div>
                        </a>
                    </li>
                    <?php foreach ( $resultado_categorias as $value ) : ?>
                    <li class="nav-item show-on-scroll" data-show-duration="500" data-show-distance="10" data-show-delay="150">
                        <a href="#" data-filter="<?= $value->nombre_categoria ?>" class="nav-link">
                            <div class="nav-link-name small"><?= $value->nombre_categoria; ?></div>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>

                <div class="pt-30 show-on-scroll" data-show-duration="800">
                    <div class="row gh-1 gv-1 isotope-grid">

                    	<?php foreach ( $resultado_trabajos as $value ) : ?>
                    	<div class="col-12 col-sm-6 isotope-item" data-filters="<?= $value->nombre_categoria; ?>" data-cursor-style="cursor-circle" data-cursor-text="ver">
                            <a class="card card-portfolio card-overlay card-hover-zoom card-image-lg card-bg-show text-white" href="trabajo.view.php?id=<?= $value->id_trabajo ?>">
                                <span class="card-img">
                                    <img src="<?= RUTA_TRABAJOS.$value->imagen_portada_trabajo ?>" alt="">
                                    <span class="background-color" style="background-color: rgba(14, 14, 14, .2);"></span>
                                </span>
                                <span class="card-img-overlay">
                                    <svg class='card-logo' width='80' height='40' viewBox='0 0 80 40' fill='none' xmlns='http://www.w3.org/2000/svg'></svg>
                                    <span class="card-title h3"><?= strtoupper( $value->nombre_trabajo ) ?></span>
                                    <span class="card-category subtitle"><?= $value->url_categoria ?></span>
                                </span>
                            </a>
                        </div>
                    	<?php endforeach ?>

                    </div>
                </div>

                <div class="text-center mt-100 mb-160">
                    <!-- <a href="#" class="btn btn-circle btn-light btn-lg">more</a> -->
                </div>
            </div>
        </div>


        <!-- Footer -->
        <?php require_once 'partials/footer.inc.php'; ?>
        <!-- Footer -->


        <!-- START: Scripts -->
        <!-- Object Fit Polyfill -->
        <script src="assets/vendor/object-fit-images/dist/ofi.min.js"></script>
        <!-- Popper -->
        <script src="assets/vendor/popper.js/dist/umd/popper.min.js"></script>
        <!-- Bootstrap -->
        <script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Bootstrap Validator -->
        <script src="assets/vendor/bootstrap-validator/dist/validator.min.js"></script>
        <!-- ImagesLoaded -->
        <script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>
        <!-- Swiper -->
        <script src="assets/vendor/swiper/dist/js/swiper.min.js"></script>
        <!-- Animejs -->
        <script src="assets/vendor/animejs/lib/anime.min.js"></script>
        <!-- Rellax -->
        <script src="assets/vendor/rellax/rellax.min.js"></script>
        <!-- Countdown -->
        <script src="assets/vendor/jquery-countdown/dist/jquery.countdown.min.js"></script>
        <!-- Moment.js -->
        <script src="assets/vendor/moment/min/moment.min.js"></script>
        <script src="assets/vendor/moment-timezone/builds/moment-timezone-with-data.min.js"></script>
        <!-- Isotope -->
        <script src="assets/vendor/isotope-layout/dist/isotope.pkgd.min.js"></script>
        <script src="assets/vendor/isotope-packery/packery-mode.pkgd.min.js"></script>
        <!-- Jarallax -->
        <script src="assets/vendor/jarallax/dist/jarallax.min.js"></script>
        <script src="assets/vendor/jarallax/dist/jarallax-video.min.js"></script>
        <!-- Fancybox -->
        <script src="assets/vendor/fancybox/dist/jquery.fancybox.min.js"></script>
        <!-- Themebau -->
        <script src="assets/js/themebau.min.js"></script>
        <!-- END: Scripts -->
    </body>
</html>